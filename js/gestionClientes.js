var clientesObtenidos;
var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";

function getClientes(){
  //var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers?$filter=Country eq 'Germany'"; //trayendo los clientes de alemania
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers"; //trayendo los clientes de alemania

  var request = new XMLHttpRequest()//par hacer uso de la ncion desde la peticion a la api
  request.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){ //(1, preparando, 2 enviando, 3 esperando la respuesta)4 que la peticion termino de cargar, 200 para ver que la peticion contesto correctamente
      //console.log(request.responseText()); //Enviando texto de respuesta de la peticion, console.table para que lo pinte en una tabla
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  }
  request.open("GET",url, true);
  request.send(); //Enviar peticion, sin body por que es solo un get

  //contacName ciudad y bandera del pais
}

function procesarClientes(){
  var JSONClientes = JSON.parse(clientesObtenidos);

  var divTabla = document.getElementById('tablaProductos');
  var tabla = document.createElement('table');
  var tbody = document.createElement('tbody');

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i = 0; i < JSONClientes.value.length; i++) {

    var nuevaFila = document.createElement("tr");

    var columnaContactName = document.createElement("td");
    columnaContactName.innerText = JSONClientes.value[i].ContactName;

    var columnaCity = document.createElement("td");
    columnaCity.innerText = JSONClientes.value[i].City;

    var columnaBandera = document.createElement("td");
    var imgBandera = document.createElement("img");
    imgBandera.classList.add("flag");

    //columnaCountry.innerText = JSONClientes.value[i].Country;
    var country = JSONClientes.value[i].Country;

    if(country == "UK")
      imgBandera.src = rutaBandera + "United-Kingdom.png";
    else{
      imgBandera.src = rutaBandera + country + ".png";
    }


    columnaBandera.appendChild(imgBandera);

    nuevaFila.appendChild(columnaContactName);
    nuevaFila.appendChild(columnaCity);
    nuevaFila.appendChild(columnaBandera);

    tbody.appendChild(nuevaFila);
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}



//Germany, Mexico, UK, Sweden, France, Spain, Switzerland, Canada, Argentina, Brazil, Austria
