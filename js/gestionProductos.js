var productosObtenidos;

function getProductos(){
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request = new XMLHttpRequest()//par hacer uso de la ncion desde la peticion a la api
  request.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){ //(1, preparando, 2 enviando, 3 esperando la respuesta)4 que la peticion termino de cargar, 200 para ver que la peticion contesto correctamente
      //console.log(request.responseText); //Enviando texto de respuesta de la peticion, console.table para que lo pinte en una tabla
      productosObtenidos=request.responseText;
      procesarProductos();
    }
  }
  request.open("GET",url, true);
  request.send(); //Enviar peticion, sin body por que es solo un get
}

function procesarProductos(){
  var JSONProductos =  JSON.parse(productosObtenidos);
  //alert(JSONProductos.value[0].ProductName);

  var divTabla = document.getElementById('tablaProductos');
  var tabla = document.createElement('table');
  var tbody = document.createElement('tbody');

  tabla.classList.add("table");
  tabla.classList.add("table-striped");
  for (var i = 0; i < JSONProductos.value.length; i++) {
    //console.log(JSONProductos.value[i].ProductName);
    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");
    columnaNombre.innerText=JSONProductos.value[i].ProductName;

    var columnaPrecio = document.createElement("td");
    columnaPrecio.innerText=JSONProductos.value[i].UnitPrice;

    var columnaStock = document.createElement("td");
    columnaStock.innerText=JSONProductos.value[i].UnitsInStock;

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);

    tbody.appendChild(nuevaFila);
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
